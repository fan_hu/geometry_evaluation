import math
import cmath
import numpy as np
import matplotlib.pyplot as plt
font = {'family': 'serif',
        'weight': 'normal', 'size': 10}
plt.rc('font', **font)

G = (1 + math.sqrt(5)) / 2
phi_0 = 2*np.pi/5

class ArrayGeometry:
    def __init__(self) -> None:
        self.strings_meta = np.zeros(0)
        pass

    def set_radius(self, radius: float):
        self.strings = radius * self.strings_meta

    def gen_hDOM_coord(self, nz=20, dz=30):
        self.hDOMs = np.zeros(shape=(len(self.strings)*nz, 3))
        for ip, p in enumerate(self.strings):
            for iz in range(nz):
                z = (iz - (nz - 1.) / 2.) * dz
                self.hDOMs[ip*nz+iz] = p[0], p[1], z

    def dump_string_coord(self, filename="string.csv"):
        np.savetxt(filename, self.strings, fmt="%.3f", delimiter=",")

    def dump_hDOM_coord(self, filename="points.csv"):
        np.savetxt(filename, self.hDOMs, fmt="%.3f", delimiter=",")


class Penrose(ArrayGeometry):
    def __init__(self, n_iter: int, radius: float = None) -> None:
        # * create wheel of red triangles around the origin
        triangles = []
        for i in range(10):
            B = cmath.rect(1, (2*i - 1) * math.pi / 10)
            C = cmath.rect(1, (2*i + 1) * math.pi / 10)
            if i % 2 == 0:
                B, C = C, B  # Make sure to mirror every second triangle
            triangles.append((0, 0j, B, C))
        
        # * iterate to get triangles
        for i in range(n_iter):
            triangles = Penrose.subdivide(triangles)
        
        # * get unique points from triangles
        self.strings_set = set()
        for color, A, B, C in triangles:
            self.strings_set.add(Penrose.String(A.real, A.imag))
            self.strings_set.add(Penrose.String(B.real, B.imag))
            self.strings_set.add(Penrose.String(C.real, C.imag))
        self.strings_meta = np.zeros(shape=(len(self.strings_set), 2))
        for i, p in enumerate(self.strings_set):
            self.strings_meta[i] = p.x, p.y
        
        # * scale strings by radius
        if radius:
            self.set_radius(radius)

    class String:
        def __init__(self, x, y) -> None:
            self.x = x
            self.y = y
            self.hash_val = self.__hash__()
        
        def __repr__(self) -> str:
            return f"({self.x:.5f}, {self.y:.5f})"

        def __hash__(self) -> int:
            return hash(round(self.x, 6)) + hash(round(self.y, 6))

        def __eq__(self, __o: object) -> bool:
            return self.hash_val == __o.hash_val

    @staticmethod
    def subdivide(triangles):
        result = []
        for color, A, B, C in triangles:
            if color == 0:
                # Subdivide red triangle
                P = A + (B - A) / G
                result += [(0, C, P, B), (1, P, C, A)]
            else:
                # Subdivide blue triangle
                Q = B + (A - B) / G
                R = B + (C - B) / G
                result += [(1, R, C, A), (1, Q, R, B), (0, R, Q, A)]
        return result

class Cube(ArrayGeometry):
    def __init__(self, num_string: int, half_side_length: float = None) -> None:
        self.strings_meta = np.zeros(shape=(num_string, 2))
        num_string_side = int(np.sqrt(num_string))
        if np.sqrt(num_string) > num_string_side:
            num_string_side += 1
        x_coord = np.linspace(-1, 1, num_string_side)
        y_coord = np.linspace(-1, 1, num_string_side)
        for idx_string in range(num_string):
            idx_x = int(idx_string / num_string_side)
            idx_y = idx_string % num_string_side
            self.strings_meta[idx_string] = x_coord[idx_x], y_coord[idx_y]
        if half_side_length:
            self.set_radius(half_side_length)

class SunFlower(ArrayGeometry):
    def __init__(self, num_string: int, radius: float = None) -> None:
        string_polar = np.zeros(shape=(num_string, 2))
        string_polar[:, 0] = np.sqrt(np.arange(1, 1+num_string)) / np.sqrt(num_string)
        string_polar[:, 1] = 2 * np.pi / G**2 * np.arange(num_string)
        self.strings_meta = np.zeros(shape=(num_string, 2))
        self.strings_meta[:, 0] = string_polar[:, 0] * np.cos(string_polar[:, 1])
        self.strings_meta[:, 1] = string_polar[:, 0] * np.sin(string_polar[:, 1])

        if radius:
            self.set_radius(radius)

def plot_geometry(points: np.ndarray, filename: str) -> None:
    plt.subplots(figsize=(6,6), dpi=500)
    plt.scatter(points[:,0], points[:,1], c="black", s=0.3)
    plt.savefig(filename)

def plot_geometry_grid(points: np.ndarray, filename: str) -> None:
    plt.subplots(figsize=(6,6), dpi=500)
    plt.scatter(points[:,0], points[:,1], c="black", s=0.3)
    grid = np.arange(-2000, 2000+0.1, 100)
    plt.xticks(grid, [])
    plt.yticks(grid, [])
    plt.grid()
    plt.savefig(filename)

if __name__ == "__main__":
    radius_penrose = 2000
    num_iter = 6
    penrose = Penrose(6, 2000)
    num_strings = len(penrose.strings)
    area_penrose = 10 * np.sin(np.pi/10) * np.cos(np.pi/10) * radius_penrose**2
    area_per_string_penrose = area_penrose / num_strings
    print(f"[penrose] num iteration: {num_iter}, num strings: {num_strings}")
    print(f"[penrose] total area: {area_penrose:.4e} m2, area per string: {area_per_string_penrose:.3e} m2")
    penrose.gen_hDOM_coord(20, 30)
    penrose.dump_string_coord("penrose_string.csv")
    penrose.dump_hDOM_coord("penrose.csv")
    plot_geometry(penrose.strings, "penrose.pdf")
    plot_geometry_grid(penrose.strings, "penrose_grid.pdf")

    # The area of a sphere with R=1950 is similar to penrose geometry of R=2000
    radius_sunflower = 1950
    area_sunflower = np.pi * radius_sunflower**2
    area_per_string_sunflower = area_sunflower / num_strings
    print(f"[sunflower] total area: {area_sunflower:.4e} m2, area per string: {area_per_string_sunflower:.3e} m2")
    sunflower = SunFlower(len(penrose.strings), radius_sunflower)
    sunflower.gen_hDOM_coord(20, 30)
    sunflower.dump_string_coord("sunflower_string.csv")
    sunflower.dump_hDOM_coord("sunflower.csv")
    plot_geometry(sunflower.strings, "sunflower.pdf")
    plot_geometry_grid(sunflower.strings, "sunflower_grid.pdf")

    # The area of a box with half_side=1714 is similar to penrose geometry of R=2000
    half_side_cubic = 1714
    area_cubic = 4 * half_side_cubic**2
    area_per_string_cubic = area_cubic / num_strings
    print(f"[cubic] total area: {area_cubic:.4e} m2, area per string: {area_per_string_cubic:.3e} m2")
    cubic = Cube(len(penrose.strings), half_side_cubic)
    cubic.gen_hDOM_coord(20, 30)
    cubic.dump_string_coord("cubic_string.csv")
    cubic.dump_hDOM_coord("cubic.csv")
    plot_geometry(cubic.strings, "cubic.pdf")
    plot_geometry_grid(cubic.strings, "cubic_grid.pdf")