from typing import List, Callable
from array_geometry import Penrose, SunFlower, Cube
import numpy as np
from scipy import interpolate
from scipy import integrate
import matplotlib.pyplot as plt
import matplotlib as mpl

def splite_by_grid(points: np.ndarray, edges: np.ndarray) -> List[List[np.ndarray]]:
    """
    Splite points in 2D space by grids
    """
    n_bins = len(edges) - 1
    grid_container = [[None for _ in range(n_bins)] for _ in range(n_bins)]
    grid_container_x = [None for _ in range(n_bins)]
    points_sort_x = points[np.argsort(points[:, 0])]

    idx_point_pre = 0
    idx_point_cur = 0
    for idx_cotainer, edge in enumerate(edges[1:]):
        while idx_point_cur < len(points_sort_x) and points_sort_x[idx_point_cur, 0] < edge:
            idx_point_cur += 1
        grid_container_x[idx_cotainer] = points_sort_x[idx_point_pre:idx_point_cur]
        idx_point_pre = idx_point_cur

    for idx_container_x, container_x in enumerate(grid_container_x):
        points_sort_y = container_x[np.argsort(container_x[:, 1])]
        idx_point_pre = 0
        idx_point_cur = 0
        for idx_cotainer, edge in enumerate(edges[1:]):
            while idx_point_cur < len(points_sort_y) and points_sort_y[idx_point_cur, 1] < edge:
                idx_point_cur += 1
            grid_container[idx_container_x][idx_cotainer] = points_sort_y[idx_point_pre:idx_point_cur]
            idx_point_pre = idx_point_cur
    
    return grid_container

def test_check_sum(grid_container: list):
    """
    Check if the sum of points in grid_container is equal to the number of points
    """
    n_points = 0
    for container_x in grid_container:
        for container_y in container_x:
            n_points += len(container_y)
    return n_points

def get_neighbor_grids(idx_x: int, idx_y: int, grid_container: list) -> List[List[np.ndarray]]:
    """
    Get the neighbor grids of a grid
    """
    n_bins = len(grid_container)
    neighbor_grids = []
    for idx_x_neighbor in range(idx_x - 1, idx_x + 2):
        for idx_y_neighbor in range(idx_y - 1, idx_y + 2):
            if idx_x_neighbor >= 0 and idx_x_neighbor < n_bins and idx_y_neighbor >= 0 and idx_y_neighbor < n_bins:
                neighbor_grids.append(grid_container[idx_x_neighbor][idx_y_neighbor])
    return neighbor_grids

def get_neighbor_grids_by_position(pos_x: int, pos_y: int, grid_edges: np.ndarray, 
                                   grid_container: list) -> List[List[np.ndarray]]:
    """
    Get the neighbor grids by position of the point
    """
    grid_size = grid_edges[1] - grid_edges[0]
    idx_x = int((pos_x - grid_edges[0]) / grid_size)
    idx_y = int((pos_y - grid_edges[0]) / grid_size)
    return get_neighbor_grids(idx_x, idx_y, grid_container)



def get_distance_spectrum(grid_container: List[List[np.ndarray]]) -> List[List[float]]:
    """
    Get the distance spectrum of points in grid_container
    The points in the neighbor grids are considered as the near points
    The distance to the near points are recorded in the distance sprctrum
    """
    distance_spectrum = []
    for idx_x, container_x in enumerate(grid_container):
        for idx_y, container_cur in enumerate(container_x):
            neighbor_grids = get_neighbor_grids(idx_x, idx_y, grid_container)
            for point in container_cur:
                distance_of_points = []
                for neighbor_grid in neighbor_grids:
                    for neighbor_point in neighbor_grid:
                        distance = np.linalg.norm(point - neighbor_point)
                        if distance > 0.1:
                            distance_of_points.append(distance)
                            if distance < 75:
                                print("We get problem")
                distance_spectrum.append(distance_of_points)
    return distance_spectrum

def draw_distance_spectrum_penrose(
        distance_spectrum: List[float], n_closest_points: int = 5, 
        distance_lim: float = 200.) -> None:
    """
    Finalized function to draw the distance spectrum of Penrose geometry
    """
    distance_spectrum_closest = []
    for distance_of_points in distance_spectrum:
        distance_of_points.sort()
        distance_spectrum_closest.append(distance_of_points[:n_closest_points])

    distance_spectrum_closest = np.sort(np.array(distance_spectrum_closest).reshape(-1))

    plt.subplots(figsize=(5,4), dpi=400)
    nums, bins, _ = plt.hist(distance_spectrum_closest, 
             bins=np.arange(0, distance_lim, 0.05), histtype="step")
    
    mask_nonzero = nums > 0
    nums_nonzero = nums[mask_nonzero]
    bins_center = (bins[:-1] + bins[1:]) / 2
    distance_nonzero = bins_center[mask_nonzero]

    for i in range(len(nums_nonzero)):
        if i == 0:
            plt.text(distance_nonzero[i]-10, nums_nonzero[i]+100, 
                    f"({distance_nonzero[i]:.1f}, {int(nums_nonzero[i]):d})")
        elif i == 2:
            plt.text(distance_nonzero[i]-15, nums_nonzero[i]+200, 
                    f"({distance_nonzero[i]:.1f}, {int(nums_nonzero[i]):d})")
        elif i == 3:
            plt.text(distance_nonzero[i]-15, nums_nonzero[i]+150, 
                    f"({distance_nonzero[i]:.1f}, {int(nums_nonzero[i]):d})")
        elif i == 4:
            plt.text(distance_nonzero[i]-20, nums_nonzero[i]+300, 
                    f"({distance_nonzero[i]:.1f}, {int(nums_nonzero[i]):d})")
        else:
            plt.text(distance_nonzero[i]-20, nums_nonzero[i]+100, 
                    f"({distance_nonzero[i]:.1f}, {int(nums_nonzero[i]):d})")
    plt.xlim(50, distance_lim)
    plt.ylim(0, 5000)
    plt.xlabel("Distance [m]")
    plt.ylabel("Counts")
    plt.savefig("distance_spectrum.pdf", bbox_inches="tight")

def draw_distance_spectrum_continuous(
        distance_spectrum: List[float], n_closest_points: int = 5, 
        distance_lim: float = 200.) -> None:
    """
    Finalized function to draw the distance spectrum of continuous geometry
    """
    distance_spectrum_closest = []
    for distance_of_points in distance_spectrum:
        distance_of_points.sort()
        distance_spectrum_closest.append(distance_of_points[:n_closest_points])

    distance_spectrum_closest = np.sort(np.array(distance_spectrum_closest).reshape(-1))

    plt.subplots(figsize=(5,4), dpi=400)
    nums, bins, _ = plt.hist(distance_spectrum_closest, 
             bins=np.arange(0, distance_lim, 1.0), histtype="step")
    
    plt.xlim(80, distance_lim)
    plt.ylim(0)
    plt.xlabel("Distance [m]")
    plt.ylabel("Counts")
    plt.savefig("distance_spectrum.pdf", bbox_inches="tight")


def get_distance_func_photon_num(d_cric: float = 2.0, d_abs: float = 25.0) -> interpolate.CubicSpline:
    """
    Get the CubicSpline function of the distance weight.
    Weight is according to the number of expected photons received by the string
    `d_cric`: the critical distance. Distance (d) will be changed to d + d_cric 
              to avoid divergency of 1 / d at d -> 0.
    `d_abs`: the absorption factor of the medium.
    """
    d = np.linspace(0, 200, 201)
    num_photons = np.zeros((len(d)))
    for idx, d_cur in enumerate(d):
        num_photons[idx] = integrate.quad(lambda x: np.exp(-(d_cur + d_cric) / d_abs / np.cos(x)), -np.pi/2, np.pi/2)[0]
    num_photons /= (d + d_cric)
    return interpolate.CubicSpline(d, num_photons)

def test_func_photon_num(d_cric: float = 2.0, d_abs: float = 25.0) -> None:
    func_photon_num = get_distance_func_photon_num(d_cric, d_abs)
    d_test = np.arange(0, 200, 0.1)
    num_photons = func_photon_num(d_test)
    plt.plot(d_test, num_photons)
    plt.yscale("log")
    plt.xlim(0, 200)


def get_property_by_position(grid_container: List[List[np.ndarray]], 
                            grid_edges: np.ndarray, centers: np.ndarray, 
                            func_property: Callable) -> np.ndarray:
    distance_crit = 200
    num_points_x = len(centers)
    num_photons = np.zeros(shape=(num_points_x, num_points_x))
    grid_size = grid_edges[1] - grid_edges[0]
    idx_grid_y_pre = -1
    idx_grid_y_cur = 0
    neighbor_grids = None
    for idx_point_x in range(num_points_x):
        pos_x = centers[idx_point_x]
        idx_grid_x_cur = int((pos_x - grid_edges[0]) / grid_size)
        for idx_point_y in range(num_points_x):
            pos_y = centers[idx_point_y]
            idx_grid_y_cur = int((pos_y - grid_edges[0]) / grid_size)
            point_neighbor = []
            if idx_grid_y_cur != idx_grid_y_pre:
                idx_grid_y_pre = idx_grid_y_cur
                neighbor_grids = get_neighbor_grids(idx_grid_x_cur, idx_grid_y_cur, grid_container)
            for grid in neighbor_grids:
                for point in grid:
                    point_neighbor.append(point)
            if len(point_neighbor) == 0:
                num_photons[idx_point_x, idx_point_y] = 0.
            else:
                point_neighbor = np.array(point_neighbor)
                distance_neighbor = np.sqrt(np.power(point_neighbor[:,0]-pos_x, 2) + 
                                            np.power(point_neighbor[:,1]-pos_y, 2))
                distance_neighbor = np.sort(distance_neighbor)
                distance_neighbor_inside = distance_neighbor[distance_neighbor < distance_crit]
                num_photons[idx_point_x, idx_point_y] = np.sum(func_property(distance_neighbor_inside))
    return num_photons

def draw_num_photons(grid_container, grid_edges):
    func_photon_num = get_distance_func_photon_num()
    resolution = 5
    r_max = 2200
    point_centers = np.linspace(-r_max, r_max, int(r_max/resolution)+1)
    num_photons = get_property_by_position(grid_container, grid_edges, point_centers, func_photon_num)

    fig = plt.figure(figsize=(8, 7.5), dpi=300)
    ax = fig.add_axes([0.15, 0.15, 0.65, 0.65])
    cax = fig.add_axes([0.83, 0.15, 0.03, 0.65])
    cmap_ = plt.get_cmap('plasma')
    norm = mpl.colors.LogNorm(1e-3, 1)
    im = ax.imshow(num_photons , cmap=cmap_, norm=norm, 
                    aspect="auto", origin="lower", 
                    extent=[point_centers[0], point_centers[-1], 
                            point_centers[0], point_centers[-1]])
    ax.set_xlabel("X Coordinate [m]")
    ax.set_ylabel("Y Coordinate [m]")
    ax.set_title(r"Propagation Efficiency")
    color_bar = fig.colorbar(im, cax=cax)
    color_bar.orientation = 'vertical'
    color_bar.set_label(r"Propagation Efficiency")
    plt.savefig("photon_number_efficiency.pdf", bbox_inches="tight")

def draw_num_strings_arount_point(grid_container, grid_edges):
    d_cric = 100
    def range_func(distance):
        return np.exp(-np.power(distance/d_cric, 2))
    resolution = 5
    r_max = 2200
    point_centers = np.linspace(-r_max, r_max, int(r_max/resolution)+1)
    num_photons = get_property_by_position(grid_container, grid_edges, point_centers, range_func)

    fig = plt.figure(figsize=(8, 7.5), dpi=300)
    ax = fig.add_axes([0.15, 0.15, 0.65, 0.65])
    cax = fig.add_axes([0.83, 0.15, 0.03, 0.65])
    cmap_ = plt.get_cmap('plasma')
    norm = mpl.colors.Normalize(2.5, 3.4)
    im = ax.imshow(num_photons , cmap=cmap_, norm=norm, 
                    aspect="auto", origin="lower", 
                    extent=[point_centers[0], point_centers[-1], 
                            point_centers[0], point_centers[-1]])
    ax.set_xlabel("X Coordinate [m]")
    ax.set_ylabel("Y Coordinate [m]")
    ax.set_title(f"Num Strings Near (kernel={d_cric}m)")
    color_bar = fig.colorbar(im, cax=cax)
    color_bar.orientation = 'vertical'
    color_bar.set_label(r"Propagation Efficiency")
    plt.savefig("strings_near_by.pdf", bbox_inches="tight")

def corridor_at_dir(points: np.ndarray, x_coord_grid: np.ndarray, 
                    d_cric: float, theta: float):
    coord_proj = np.cos(theta)*points[:,0] + np.sin(theta)*points[:,1]
    field = np.zeros(len(x_coord_grid))
    for x_coord_point in coord_proj:
        field += np.exp(-(np.power((x_coord_grid-x_coord_point)/d_cric, 2)))        
    return field

def select_outer_points_penrose(points: np.ndarray, d_cric=1650):
    mask_inner = np.ones(shape=len(points), dtype=bool)
    for theta_axis in np.arange(0, np.pi, np.pi/5):
        coord_proj = np.cos(theta_axis)*points[:,0] + np.sin(theta_axis)*points[:,1]
        mask_inner &= np.abs(coord_proj) <= d_cric
    mask_outer = np.invert(mask_inner)
    return points[mask_outer]

def select_outer_points_sphere(points: np.ndarray, d_cric=1800):
    radius = np.sqrt(np.power(points[:,0], 2) + np.power(points[:,1], 2))
    mask_outer = radius >= d_cric
    return points[mask_outer]

def select_outer_points_box(points: np.ndarray, d_cric=1800):
    mask_outer = np.abs(points[:,0]) >= d_cric
    mask_outer |= np.abs(points[:,1]) >= d_cric
    return points[mask_outer]



def plot_points_outer(points: np.ndarray, points_outer: np.ndarray):
    plt.subplots(figsize=(6,6), dpi=500)
    plt.scatter(points[:,0], points[:,1], c="black", s=0.3)
    plt.scatter(points_outer[:,0], points_outer[:,1], c="red", s=0.5)
    plt.savefig("points_outer.pdf")

def draw_corridor(points_outer: np.ndarray):
    r_max = 1500
    resolution = 1
    d_cric = 35
    threshold = 1.0
    coord_grid = np.linspace(-r_max, r_max, int(2*r_max/resolution)+1)
    theta_arr_deg = np.arange(0, 180, 2)
    theta_arr = theta_arr_deg / 180 * np.pi
    ratio_sub_threshold = np.zeros(len(theta_arr_deg))
    for idx, theta in enumerate(theta_arr):
        mask_positive = (-np.sin(theta) * points_outer[:, 0] + np.cos(theta) * points_outer[:, 1]) > 0.
        points_outer_positive = points_outer[mask_positive]
        field = corridor_at_dir(points_outer_positive, coord_grid, d_cric, theta)
        ratio_sub_threshold[idx] = np.sum(field < threshold) / len(field)
        # plt.subplots(figsize=(4.5, 3.5), dpi=300)
        # plt.plot(coord_grid, field)
        # plt.hlines(threshold, coord_grid[0], coord_grid[-1],
        #            linestyles="-.", colors="black")
        # plt.xlim(-1500, 1500)
        # plt.ylim(0, 4.5)
        # plt.xlabel("Projected Coordinate [m]")
        # plt.ylabel(f"Nearby strings (kernal={d_cric}m)")
        # plt.title(f"View at {theta_arr_deg[idx]:.2f} degree")
        # plt.savefig(f"corridor_{idx}th.pdf", bbox_inches="tight")
    plt.subplots(figsize=(4.5, 3.5), dpi=300)
    plt.plot(theta_arr_deg, ratio_sub_threshold)
    plt.xlim(0, 180)
    plt.ylim(0, 0.3)
    plt.xlabel("Incident Angle [degree]")
    plt.ylabel(f"Subthreshold Ratio (threshold = {threshold:.1f})")
    plt.savefig("corridor_angle.pdf", bbox_inches="tight")

def draw_corridor_full_array(points: np.ndarray):
    r_max = 2200
    resolution = 1
    d_cric = 35
    coord_grid = np.linspace(-r_max, r_max, int(2*r_max/resolution)+1)
    theta_arr_deg = np.arange(0, 180, 2)
    theta_arr = theta_arr_deg / 180 * np.pi
    for idx, theta in enumerate(theta_arr):
        field = corridor_at_dir(points, coord_grid, d_cric, theta)
        plt.subplots(figsize=(4.5, 3.5), dpi=300)
        plt.plot(coord_grid, field)
        plt.hlines(15.0, coord_grid[0], coord_grid[-1],
                   linestyles="-.", colors="black")
        plt.xlim(-2200, 2200)
        plt.ylim(0, 30)
        plt.xlabel("Projected Coordinate [m]")
        plt.ylabel(f"Nearby strings (kernal={d_cric}m)")
        plt.title(f"View at {theta_arr_deg[idx]:.2f} degree")
        plt.savefig(f"corridor_full_array_{idx}th.pdf", bbox_inches="tight")


if __name__ == "penrose":
# if __name__ == "__main__":
    radius = 2000
    n_iter = 6
    bin_size = 200
    edge_size = radius + bin_size / 2
    num_edges = int(2*radius / bin_size) + 1
    penrose = Penrose(n_iter, radius=radius)
    points = penrose.strings
    grid_edges = np.linspace(-edge_size, edge_size, num_edges)

    grid_container = splite_by_grid(points, grid_edges)
    n_points = test_check_sum(grid_container)
    print(f"Number of points: {n_points}")

    distance_spectrum = get_distance_spectrum(grid_container)
    draw_distance_spectrum_penrose(distance_spectrum)

    draw_num_photons(grid_container, grid_edges)

    draw_num_strings_arount_point(grid_container, grid_edges)

    points_outer = select_outer_points_penrose(points)
    # len(points_outer) = 320
    plot_points_outer(points, points_outer)
    draw_corridor(points_outer)

    draw_corridor_full_array(points)

if __name__ == "sunflower":
# if __name__ == "__main__":
    radius_edge = 2000
    bin_size = 200
    num_edges = int(2*radius_edge / bin_size) + 1
    edge_size = radius_edge + bin_size / 2
    grid_edges = np.linspace(-edge_size, edge_size, num_edges)

    radius_sunflower = 1960
    # pi * 1960**2 is similar to penrose geometry of R=2000
    sunflower = SunFlower(1211, radius_sunflower)
    points = sunflower.strings

    grid_container = splite_by_grid(points, grid_edges)
    n_points = test_check_sum(grid_container)
    print(f"Number of points: {n_points}")

    distance_spectrum = get_distance_spectrum(grid_container)

    draw_distance_spectrum_continuous(distance_spectrum)

    draw_num_photons(grid_container, grid_edges)

    draw_num_strings_arount_point(grid_container, grid_edges)

    # this magic number 1682 is to make the len(points_outer) = 320
    points_outer = select_outer_points_sphere(points, d_cric=1682)
    len(points_outer)
    plot_points_outer(points, points_outer)

    draw_corridor(points_outer)

    draw_corridor_full_array(points)

if __name__ == "box":
    radius_edge = 2000
    bin_size = 200
    num_edges = int(2*radius_edge / bin_size) + 1
    edge_size = radius_edge + bin_size / 2
    grid_edges = np.linspace(-edge_size, edge_size, num_edges)

    half_side_cube = 1714
    cube = Cube(1211, half_side_cube)
    points = cube.strings
    
    grid_container = splite_by_grid(points, grid_edges)
    n_points = test_check_sum(grid_container)
    print(f"Number of points: {n_points}")

    distance_spectrum = get_distance_spectrum(grid_container)

    draw_distance_spectrum_continuous(distance_spectrum)

    draw_num_photons(grid_container, grid_edges)

    draw_num_strings_arount_point(grid_container, grid_edges)

    # this magic number 1682 is to make the len(points_outer) = 334
    # for cube geometry, we can not make is the same as 320
    points_outer = select_outer_points_box(points, d_cric=1500)
    len(points_outer)
    plot_points_outer(points, points_outer)

    draw_corridor(points_outer)

    draw_corridor_full_array(points)
