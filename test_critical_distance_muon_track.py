import numpy as np
from scipy import optimize
import matplotlib.pyplot as plt

d = np.arange(5, 100, 1)

def get_num_photons(d, light_yield, num_cric):
    """
    light yiled in unit of photons/cm2
    1 TeV muon: 1e3 (almost minimum ionization muon track)
    """
    num_photons = light_yield * 1.2 / (2*np.pi * d) * np.exp(-np.sqrt(2)*d/30)
    return num_photons - num_cric

rst = optimize.fsolve(get_num_photons, x0=40, args=(1e3, 1.0))[0]
print(f"intersection = {rst:.2f} m")

# intersection point is about 30

# to account for vertical spacing of 30m/2=15m
print(np.sqrt(rst**2 - 15**2))